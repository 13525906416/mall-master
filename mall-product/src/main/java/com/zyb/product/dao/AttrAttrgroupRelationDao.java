package com.zyb.product.dao;

import com.zyb.product.entity.AttrAttrgroupRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 属性&属性分组关联
 * 
 * @author zhaoyb
 * @email zyb068@aliyun.com
 * @date 2023-06-13 00:35:09
 */
@Mapper
public interface AttrAttrgroupRelationDao extends BaseMapper<AttrAttrgroupRelationEntity> {

    void deleteBatchRelations(List<AttrAttrgroupRelationEntity> list);
}
