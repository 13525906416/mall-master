package com.zyb.product.entity.vo;

/**
 * @ClassName Attr
 * @Author zhaoyb
 * @Date 2023/10/24
 * @Version 1.0
 */
@lombok.Data
public class Attr {
    private long attrId;
    private String attrName;
    private String attrValue;
}