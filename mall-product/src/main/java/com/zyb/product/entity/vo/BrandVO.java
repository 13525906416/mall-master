package com.zyb.product.entity.vo;

import lombok.Data;

/**
 * @ClassName BrandVO
 * @Author zhaoyb
 * @Date 2023/10/19
 * @Version 1.0
 */
@Data
public class BrandVO {
    private Long brandId;

    private String brandName;
}
