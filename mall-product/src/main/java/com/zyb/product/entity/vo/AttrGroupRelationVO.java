package com.zyb.product.entity.vo;

import lombok.Data;

/**
 * @ClassName AttrGroupRelationVO
 * @Author zhaoyb
 * @Date 2023/9/3
 * @Version 1.0
 */
@Data
public class AttrGroupRelationVO {
    private Long attrId;

    private Long attrGroupId;
}
