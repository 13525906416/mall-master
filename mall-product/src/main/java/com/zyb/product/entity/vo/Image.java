package com.zyb.product.entity.vo;

/**
 * @ClassName Image
 * @Author zhaoyb
 * @Date 2023/10/24
 * @Version 1.0
 */
@lombok.Data
public class Image {
    private Integer defaultImg;
    private String imgUrl;
}