package com.zyb.product.entity.vo;

import com.zyb.coupon.entity.vo.MemberPrice;

import java.math.BigDecimal;
import java.util.List;

/**
 * @ClassName Skus
 * @Author zhaoyb
 * @Date 2023/10/24
 * @Version 1.0
 */
@lombok.Data
public class Skus {
    private List<Attr> attr;
    private long countStatus;
    private List<String> descar;
    private BigDecimal discount;
    private BigDecimal fullCount;
    private BigDecimal fullPrice;
    private List<Image> images;
    private List<MemberPrice> memberPrice;
    private BigDecimal price;
    private long priceStatus;
    private long reducePrice;
    private String skuName;
    private String skuSubtitle;
    private String skuTitle;
}