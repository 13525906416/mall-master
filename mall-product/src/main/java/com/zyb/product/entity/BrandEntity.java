package com.zyb.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

import com.zyb.common.validator.annotation.ListValue;
import com.zyb.common.validator.group.AddGroup;
import com.zyb.common.validator.group.UpdateGroup;
import com.zyb.common.validator.group.UpdateStatusGroup;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;

/**
 * 品牌
 * 
 * @author zhaoyb
 * @email zyb068@aliyun.com
 * @date 2023-06-13 00:35:09
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 品牌id
	 */
	@TableId
	@NotNull(message = "修改必须指定品牌id",groups = {UpdateGroup.class})
	@Null(message = "新增不能指定id",groups = {AddGroup.class})
	private Long brandId;
	/**
	 * 品牌名
	 * NotBlank 不能为空并且至少包含一个非空格字符   must not be null and must contain at least one non-whitespace character
	 */
	@NotBlank(message = "品牌名称不能为空",groups = {AddGroup.class, UpdateGroup.class})
	private String name;
	/**
	 * 品牌logo地址
	 * NotEmpty
	 */
	@URL(message = "logo必须是一个合法的url地址",groups = {AddGroup.class, UpdateGroup.class})
	@NotEmpty(groups = {AddGroup.class, UpdateGroup.class})
	private String logo;
	/**
	 * 介绍
	 */
	private String descript;
	/**
	 * 显示状态[0-不显示；1-显示]
	 * 自定义校验注解
	 */
	@NotNull(groups = {AddGroup.class, UpdateGroup.class})
	@ListValue(vals = {0,1},groups = {AddGroup.class, UpdateStatusGroup.class})
	private Integer showStatus;
	/**
	 * 检索首字母
	 * Pattern 自定义校验规则
	 */
	@NotEmpty(groups = {AddGroup.class, UpdateGroup.class})
	@Pattern(regexp = "^[a-zA-Z]$",message = "检索首字母必须是一个字母",groups = {AddGroup.class, UpdateGroup.class})
	private String firstLetter;
	/**
	 * 排序
	 * NotNull 可以对任意类型校验，NotEmpty不可以   The annotated element must not be null. Accepts any type
	 */
	@NotNull(groups ={AddGroup.class, UpdateGroup.class})
	@Min(value = 0,message = "排序字段必须是一个正整数",groups = {AddGroup.class, UpdateGroup.class})
	private Integer sort;

}
