package com.zyb.product.entity.vo;

import java.math.BigDecimal;

/**
 * @ClassName Bounds
 * @Author zhaoyb
 * @Date 2023/10/24
 * @Version 1.0
 */
@lombok.Data
public class Bounds {
    private BigDecimal buyBounds;
    private BigDecimal growBounds;
}