package com.zyb.product.entity.vo;

import lombok.Data;

/**
 * @ClassName Discount
 * @Author zhaoyb
 * @Date 2023/10/24
 * @Version 1.0
 */

@Data
public class Discount {
    public Double doubleValue;
    public Long integerValue;
}