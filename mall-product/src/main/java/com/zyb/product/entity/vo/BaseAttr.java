package com.zyb.product.entity.vo;

/**
 * @ClassName BaseAttr
 * @Author zhaoyb
 * @Date 2023/10/24
 * @Version 1.0
 */
@lombok.Data
public class BaseAttr {
    private long attrId;
    private String attrValues;
    private Integer showDesc;
}
