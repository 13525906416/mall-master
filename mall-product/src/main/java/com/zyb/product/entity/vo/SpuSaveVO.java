package com.zyb.product.entity.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * @ClassName SpuSaveVO
 * @Author zhaoyb
 * @Date 2023/10/24
 * @Version 1.0
 */
@lombok.Data
public class SpuSaveVO {
    private List<BaseAttr> baseAttrs;
    private Bounds bounds;
    private long brandId;
    private long catalogId;
    private List<String> decript;
    private List<String> images;
    private long publishStatus;
    private List<Skus> skus;
    private String spuDescription;
    private String spuName;
    private BigDecimal weight;
}
