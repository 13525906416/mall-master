package com.zyb.product.entity.vo;

import com.zyb.product.entity.dto.AttrEntityDTO;
import lombok.Data;

/**
 * @ClassName AttrRespVO
 * @Author zhaoyb
 * @Date 2023/8/29
 * @Version 1.0
 */
@Data
public class AttrRespVO extends AttrEntityDTO {
    private String catelogName;

    private String groupName;

    private Long[] catelogPath;
}
