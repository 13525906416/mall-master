package com.zyb.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyb.common.utils.PageUtils;
import com.zyb.product.entity.BrandEntity;

import java.util.Map;

/**
 * 品牌
 *
 * @author zhaoyb
 * @email zyb068@aliyun.com
 * @date 2023-06-13 00:35:09
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageByKey(Map<String, Object> params);

    void updateByIdDetail(BrandEntity brand);
}

