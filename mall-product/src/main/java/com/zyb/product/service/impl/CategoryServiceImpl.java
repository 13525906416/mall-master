package com.zyb.product.service.impl;

import com.zyb.product.entity.dto.CategoryEntityDTO;
import com.zyb.product.service.CategoryBrandRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyb.common.utils.PageUtils;
import com.zyb.common.utils.Query;

import com.zyb.product.dao.CategoryDao;
import com.zyb.product.entity.CategoryEntity;
import com.zyb.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    //手敲练习一下树的封装 ,可以使用双重for  或自己喜欢的方式
    @Override
    public List<CategoryEntityDTO> queryWithTree() {
        //1. 查出所有分类
        List<CategoryEntityDTO> list = this.baseMapper.queryByMap();
        //2. 封装树形结构
        //2.1找到所有的一级分类    一级分类 == 父id=0
        List<CategoryEntityDTO> entityMenu = list.stream().filter(categoryEntity -> {
            return categoryEntity.getParentCid() == 0;
        }).map((menu) ->{
            menu.setChildren(getChildren(menu,list));
            return menu;
        }).sorted((menu1,menu2) -> {
            return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
        }).collect(Collectors.toList());
        return entityMenu;
    }

    @Override
    public void removeLogicByIds(List<Long> ids) {
        //TODO 检查当前删除的菜单是否被其他地方引用
        baseMapper.deleteBatchIds(ids);
    }

    /**
     * 找到完整[catId]
     * @param catelogId
     * @return
     */
    @Override
    public Long[] getCatelogPath(Long catelogId) {
        List<Long> paths = new ArrayList<>();
        //递归查找父亲
        findParentPath(catelogId,paths);
        return  paths.toArray(new Long[paths.size()]);
    }

    @Transactional
    @Override
    public void updateCascade(CategoryEntity category) {
        this.updateById(category);
        categoryBrandRelationService.updateCategory(category.getCatId(),category.getName());
    }

    private List<Long> findParentPath(Long catId,List<Long> paths){

        //逆序输出
//        CategoryEntity byId = this.getById(catId);
//        if (byId.getParentCid() !=0){
//            findParentPath(byId.getParentCid(),paths);
//        }

        if (catId ==0){
            return paths;
        }else {
            findParentPath(this.getById(catId).getParentCid(),paths);
        }
        paths.add(catId);
        return paths;
    }

    private List<CategoryEntityDTO> getChildren(CategoryEntityDTO root,List<CategoryEntityDTO> all){
        List<CategoryEntityDTO> list = all.stream().filter(categoryEntityDTO -> {
            return categoryEntityDTO.getParentCid().equals(root.getCatId()); //当前节点的id是否和父ID相同
        }).map(categoryDTO -> {
            categoryDTO.setChildren(getChildren(categoryDTO, all));
            return categoryDTO;
        }).sorted((menu1,menu2) -> {
            return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
        }).collect(Collectors.toList());
        return list;
    }

}