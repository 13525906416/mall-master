package com.zyb.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zyb.common.utils.Constant;
import com.zyb.common.utils.ProductConstant;
import com.zyb.product.dao.AttrAttrgroupRelationDao;
import com.zyb.product.dao.AttrGroupDao;
import com.zyb.product.dao.CategoryDao;
import com.zyb.product.entity.AttrAttrgroupRelationEntity;
import com.zyb.product.entity.AttrGroupEntity;
import com.zyb.product.entity.CategoryEntity;
import com.zyb.product.entity.dto.AttrEntityDTO;
import com.zyb.product.entity.vo.AttrGroupRelationVO;
import com.zyb.product.entity.vo.AttrGroupWithAttrsVO;
import com.zyb.product.entity.vo.AttrRespVO;
import com.zyb.product.service.CategoryService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyb.common.utils.PageUtils;
import com.zyb.common.utils.Query;

import com.zyb.product.dao.AttrDao;
import com.zyb.product.entity.AttrEntity;
import com.zyb.product.service.AttrService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestParam;


@Service("attrService")
public class AttrServiceImpl extends ServiceImpl<AttrDao, AttrEntity> implements AttrService {
    @Autowired
    private AttrAttrgroupRelationDao attrAttrgroupRelationDao;

    @Autowired
    private AttrGroupDao attrGroupDao;

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private CategoryService categoryService;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                new QueryWrapper<AttrEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    @Transactional
    public void saveAttr(AttrEntityDTO attrDto) {
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attrDto, attrEntity);
        this.save(attrEntity);

        //保存关联关系
        if (attrDto.getAttrType() == ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode()){
            AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = new AttrAttrgroupRelationEntity();
            attrAttrgroupRelationEntity.setAttrGroupId(attrDto.getAttrGroupId());
            attrAttrgroupRelationEntity.setAttrId(attrEntity.getAttrId());
            attrAttrgroupRelationDao.insert(attrAttrgroupRelationEntity);
        }

    }

    @Override
    public PageUtils queryBaseAttrPage(Map<String, Object> params, Long catelogId, String attrType) {
        QueryWrapper<AttrEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("attr_type", "base".equalsIgnoreCase(attrType) ? ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode() : ProductConstant.AttrEnum.ATTR_TYPE_SALE.getCode());
        if (catelogId != 0) {
            wrapper.eq("catelog_id", catelogId);
        }
        String key = (String) params.get("key");
        if (!StringUtils.isEmpty(key)) {
            wrapper.and((wrapper1) -> {
                wrapper1.eq("attr_id", key).or()
                        .like("attr_name", key);
            });
        }
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                wrapper
        );
        PageUtils pageUtils = new PageUtils(page);
        List<AttrEntity> records = page.getRecords();
        List<AttrRespVO> respVOS = records.stream().map((attrEntity) -> {
            AttrRespVO attrRespVO = new AttrRespVO();
            BeanUtils.copyProperties(attrEntity, attrRespVO);

            if ("base".equalsIgnoreCase(attrType)) {
                AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = attrAttrgroupRelationDao.selectOne(new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", attrEntity.getAttrId()));
                if (attrAttrgroupRelationEntity != null) {
                    AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(attrAttrgroupRelationEntity.getAttrGroupId());
                    attrRespVO.setGroupName(attrGroupEntity.getAttrGroupName());
                }
            }

            CategoryEntity categoryEntity = categoryDao.selectById(attrEntity.getCatelogId());
            if (categoryEntity != null) {
                attrRespVO.setCatelogName(categoryEntity.getName());
            }
            return attrRespVO;
        }).collect(Collectors.toList());
        pageUtils.setList(respVOS);
        return pageUtils;
    }

    @Override
    public AttrRespVO getAttrInfo(Long attrId) {
        AttrRespVO attrRespVO = new AttrRespVO();
        AttrEntity byId = this.getById(attrId);
        BeanUtils.copyProperties(byId, attrRespVO);
        AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = attrAttrgroupRelationDao.selectOne(new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", attrId));

        if (byId.getAttrType() ==ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode()){
            //分组信息
            if (attrAttrgroupRelationEntity != null) {
                attrRespVO.setAttrGroupId(attrAttrgroupRelationEntity.getAttrGroupId());
                AttrGroupEntity attrGroup = attrGroupDao.selectById(attrAttrgroupRelationEntity.getAttrGroupId());
                if (attrGroup != null) {
                    attrRespVO.setGroupName(attrGroup.getAttrGroupName());
                }
            }
        }


        //设置分类信息
        Long catelogId = byId.getCatelogId();
        Long[] catelogPath = categoryService.getCatelogPath(catelogId);
        attrRespVO.setCatelogPath(catelogPath);
        CategoryEntity categoryEntity = categoryDao.selectById(catelogId);
        if (categoryEntity != null) {
            attrRespVO.setCatelogName(categoryEntity.getName());
        }
        return attrRespVO;
    }

    @Transactional
    @Override
    public void updateAttr(AttrRespVO attr) {
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attr, attrEntity);
        this.updateById(attrEntity);

        if (attrEntity.getAttrType() == ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode()){
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            relationEntity.setAttrId(attr.getAttrId());
            relationEntity.setAttrGroupId(attr.getAttrGroupId());
            //大于0 ，说明有数据为修改
            Long count = attrAttrgroupRelationDao.selectCount(new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", attr.getAttrId()));
            if (count > 0) {
                //修改分组关联
                attrAttrgroupRelationDao.update(relationEntity, new UpdateWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", relationEntity.getAttrId()));
            } else {
                //小于等于0 ，说明没数据为新增
                attrAttrgroupRelationDao.insert(relationEntity);
            }
        }

    }

    /**
     * 根据分组id查找关联的所有基本属性
     * @param attrgroupId
    select attr_id, attr_name, value_select
    from pms_attr
    where attr_id in
    (select r.attr_id
    from pms_attr_group g
    left join gulimall_pms.pms_attr_attrgroup_relation r
    on g.attr_group_id = r.attr_group_id and g.attr_group_id = ?);  //?代表attrgroupId
     */
    @Override
    public List<AttrEntity> getAttrRelation(Long attrgroupId) {
        List<AttrAttrgroupRelationEntity> attrgroupRelationEntities = attrAttrgroupRelationDao.selectList(new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_group_id", attrgroupId));
        List<Long> attrIdList = attrgroupRelationEntities.stream().map((attrAttrgroupRelationEntity -> {
            return attrAttrgroupRelationEntity.getAttrId();
        })).collect(Collectors.toList());
        if (attrIdList ==null || attrIdList.size() ==  0){
            return null;
        }
        List<AttrEntity> attrEntities = this.listByIds(attrIdList);
        return attrEntities;
    }

    @Override
    @Transactional
    public void deleteRelation(AttrGroupRelationVO[] vos) {
        List<AttrAttrgroupRelationEntity> list = Arrays.asList(vos).stream().map((item) -> {
            AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = new AttrAttrgroupRelationEntity();
            attrAttrgroupRelationEntity.setAttrId(item.getAttrId());
            attrAttrgroupRelationEntity.setAttrGroupId(item.getAttrGroupId());
            return attrAttrgroupRelationEntity;
        }).collect(Collectors.toList());

        attrAttrgroupRelationDao.deleteBatchRelations(list);
    }

    /**
     * 获取当前分组没有关联的属性
     * @param params
     * @param attrgroupId
    select *
    from pms_attr where attr_id not in (
    select attr_id
    from pms_attr_attrgroup_relation where attr_group_id in (
    select attr_group_id
    from pms_attr_group where catelog_id in (select catelog_id
    from pms_attr_group where attr_group_id = 1)
    )
    )
    and catelog_id in (select catelog_id
    from pms_attr_group where attr_group_id = 71)
    and attr_type = 1;
     * 优化后
     * SELECT *
     * FROM pms_attr
     * LEFT JOIN pms_attr_attrgroup_relation ON pms_attr.attr_id = pms_attr_attrgroup_relation.attr_id
     * LEFT JOIN pms_attr_group ON pms_attr_attrgroup_relation.attr_group_id = pms_attr_group.attr_group_id
     * WHERE pms_attr.catelog_id = (SELECT catelog_id FROM pms_attr_group WHERE attr_group_id = 7)
     * AND pms_attr.attr_type = 1
     * AND pms_attr_attrgroup_relation.attr_id IS NULL;
     */
    @Override
    public PageUtils getNoattrPage(Map<String, Object> params, Long attrgroupId) {
        //当前分组只能关联自己所属分类的属性
        AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(attrgroupId);
        Long catelogId = attrGroupEntity.getCatelogId();
        //当前分组只能关联没有被别的分组引用的属性
        List<AttrGroupEntity> group = attrGroupDao.selectList(
                new QueryWrapper<AttrGroupEntity>()
                        .eq("catelog_id", catelogId));
        List<Long> attrGroupIds = group.stream().map((item) -> {
            return item.getAttrGroupId();
        }).collect(Collectors.toList());
        //TODO
        if (!CollectionUtils.isEmpty(attrGroupIds)){

        }
        List<AttrAttrgroupRelationEntity> attrgroupRelationEntities = attrAttrgroupRelationDao
                .selectList(new QueryWrapper<AttrAttrgroupRelationEntity>()
                                .in("attr_group_id", attrGroupIds));
        List<Long> attrIds = attrgroupRelationEntities.stream().map((item) -> {
            return item.getAttrId();
        }).collect(Collectors.toList());
        String key = (String) params.get("key");

        QueryWrapper<AttrEntity> attrEntityQueryWrapper = new QueryWrapper<AttrEntity>()
                .eq("catelog_id", catelogId)
                .eq("attr_type", ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode());
        if (attrIds !=null && attrIds.size()>0){
            attrEntityQueryWrapper.notIn("attr_id", attrIds);
        }
        if (!StringUtils.isEmpty(key)){
            attrEntityQueryWrapper.and((w)->{
                w.eq("attr_id",key).or().like("attr_name",key);
            });
        }
        IPage<AttrEntity> page = this.page(new Query<AttrEntity>().getPage(params), attrEntityQueryWrapper);

        return new PageUtils(page);
    }

}