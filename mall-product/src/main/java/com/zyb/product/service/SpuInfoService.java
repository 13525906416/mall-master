package com.zyb.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyb.common.utils.PageUtils;
import com.zyb.product.entity.SpuInfoDescEntity;
import com.zyb.product.entity.SpuInfoEntity;
import com.zyb.product.entity.vo.SpuSaveVO;

import java.util.List;
import java.util.Map;

/**
 * spu信息
 *
 * @author zhaoyb
 * @email zyb068@aliyun.com
 * @date 2023-06-13 00:35:09
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSpuInfo(SpuSaveVO spuInfo);

    void saveBaseSpuInfo(SpuInfoEntity spuInfoEntity);

    void saveSpuInfoDesc(SpuInfoDescEntity spuInfoDescEntity);

}

