package com.zyb.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyb.common.utils.PageUtils;
import com.zyb.product.entity.AttrEntity;
import com.zyb.product.entity.dto.AttrEntityDTO;
import com.zyb.product.entity.vo.AttrGroupRelationVO;
import com.zyb.product.entity.vo.AttrGroupWithAttrsVO;
import com.zyb.product.entity.vo.AttrRespVO;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author zhaoyb
 * @email zyb068@aliyun.com
 * @date 2023-06-13 00:35:09
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttr(AttrEntityDTO attrDto);

    PageUtils queryBaseAttrPage(Map<String, Object> params, Long catelogId,String attrType);

    AttrRespVO getAttrInfo(Long attrId);

    void updateAttr(AttrRespVO attr);

    List<AttrEntity> getAttrRelation(Long attrgroupId);

    void deleteRelation(AttrGroupRelationVO[] vos);

    PageUtils getNoattrPage(Map<String, Object> params, Long attrgroupId);
}

