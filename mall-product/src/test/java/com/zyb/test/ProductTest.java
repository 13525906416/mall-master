package com.zyb.test;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zyb.product.ProductApplication;
import com.zyb.product.entity.BrandEntity;
import com.zyb.product.service.BrandService;
import com.zyb.product.service.CategoryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @ClassName ProductTest
 * @Author zhaoyb
 * @Date 2023/6/20
 * @Version 1.0
 */
@SpringBootTest(classes = ProductApplication.class)
public class ProductTest {
    @Autowired
    private BrandService brandService;

    @Autowired
    private CategoryService categoryService;

    @Test
    public void testBrandSave(){
        BrandEntity brand = new BrandEntity();
        brand.setName("zTest");
        brandService.save(brand);
    }

    @Test
    public void testBrandUpdate(){
        BrandEntity brand = new BrandEntity();
        brand.setBrandId(38L);
        brand.setDescript("zTestUpdate");
        brandService.updateById(brand);
    }

    @Test
    public void testBrandQuery(){
        List<BrandEntity> list = brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id", 38L));
        for (BrandEntity brand : list){
            System.out.println(brand);
        }
    }

    @Test
    public void testCatelogPath(){
        categoryService.getCatelogPath(225L);
    }
}
