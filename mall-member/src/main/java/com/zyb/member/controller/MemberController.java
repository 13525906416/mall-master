package com.zyb.member.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.zyb.coupon.client.RemoteCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zyb.member.entity.MemberEntity;
import com.zyb.member.service.MemberService;
import com.zyb.common.utils.PageUtils;
import com.zyb.common.utils.R;

import javax.annotation.Resource;


/**
 * 会员
 *
 * @author zhaoyb
 * @email zyb068@aliyun.com
 * @date 2023-06-26 22:00:06
 */
@RestController
@RefreshScope
@RequestMapping("member/member")
public class MemberController {
    @Autowired
    private MemberService memberService;

    @Resource
    private RemoteCouponService remoteCouponService;


    @Value("${mall.name}")
    private String name;

    //测试feign的接口，可忽略
    @RequestMapping("/coupons")
    public R RemoteMemberCoupons(){
        MemberEntity memberEntity = new MemberEntity();
        memberEntity.setNickname("z张三");
        R memberCoupons = remoteCouponService.memberCoupons();
        //测试配置中心
        if (name == null || name.equals("")){
            name = "default";
        }
        return R.ok().put("member",memberEntity).put("coupons",memberCoupons.get("coupons")).put("name",name);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("member:member:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = memberService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("member:member:info")
    public R info(@PathVariable("id") Long id){
		MemberEntity member = memberService.getById(id);

        return R.ok().put("member", member);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("member:member:save")
    public R save(@RequestBody MemberEntity member){
		memberService.save(member);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("member:member:update")
    public R update(@RequestBody MemberEntity member){
		memberService.updateById(member);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("member:member:delete")
    public R delete(@RequestBody Long[] ids){
		memberService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
