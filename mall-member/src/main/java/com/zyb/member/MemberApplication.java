package com.zyb.member;

import com.zyb.common.feign.annotation.EnableZhaoybFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@EnableZhaoybFeignClients(basePackages = {"com.zyb.coupon","com.zyb.member"})
public class MemberApplication {
    public static void main(String[] args) {
        SpringApplication.run(MemberApplication.class,args);
    }
}