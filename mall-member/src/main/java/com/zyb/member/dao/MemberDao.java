package com.zyb.member.dao;

import com.zyb.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author zhaoyb
 * @email zyb068@aliyun.com
 * @date 2023-06-26 22:00:06
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
