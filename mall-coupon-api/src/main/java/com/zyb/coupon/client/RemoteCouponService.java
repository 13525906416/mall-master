package com.zyb.coupon.client;

import com.zyb.common.utils.R;
import com.zyb.common.utils.ServiceNameConstants;
import com.zyb.coupon.entity.to.SkuReductionTO;
import com.zyb.coupon.entity.to.SpuBoundsTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;

@FeignClient(contextId = "RemoteCouponService",value = ServiceNameConstants.MALL_COUPON)
public interface RemoteCouponService {

    //测试feign的接口，可忽略
    @RequestMapping("/coupon/coupon/member/list")
    public R memberCoupons();

    @PostMapping("/coupon/spubounds/save")
    public R saveSpuBounds(@RequestBody SpuBoundsTO spuBoundsTO);

    @PostMapping("/coupon/skufullreduction/saveInfo")
    public R saveSkuReduction(@RequestBody SkuReductionTO skuReductionTO);

}
