package com.zyb.coupon.entity.to;

import com.zyb.coupon.entity.vo.MemberPrice;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @ClassName SkuReductionTO
 * @Author zhaoyb
 * @Date 2023/10/25
 * @Version 1.0
 */
@Data
public class SkuReductionTO {
    private Long skuId;
    private BigDecimal fullCount;
    private BigDecimal fullPrice;
    private Integer countStatus;
    private Integer priceStatus;
    private Integer reducePrice;
    private BigDecimal discount;
    private List<MemberPrice> memberPrices;
}
