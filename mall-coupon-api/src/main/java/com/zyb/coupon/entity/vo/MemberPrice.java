package com.zyb.coupon.entity.vo;

import java.math.BigDecimal;

/**
 * @ClassName MemberPrice
 * @Author zhaoyb
 * @Date 2023/10/24
 * @Version 1.0
 */
@lombok.Data
public class MemberPrice {
    private long id;
    private String name;
    private BigDecimal price;
}
