package com.zyb.coupon.entity.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName SpuBoundsTO
 * @Author zhaoyb
 * @Date 2023/10/25
 * @Version 1.0
 */
@Data
public class SpuBoundsTO {
    private Long spuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;
}
