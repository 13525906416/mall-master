package com.zyb.gateway.config;

import com.zyb.gateway.filter.ZhaoybRequestGlobalFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
/**
 * @ClassName GatewayConfiguration
 * @Author zhaoyb
 * @Date 2023/7/18
 * @Version 1.0
 */
@Configuration(proxyBeanMethods = false)
public class GatewayConfiguration {
    @Bean
    public ZhaoybRequestGlobalFilter zhaoybRequestGlobalFilter() {
        return new ZhaoybRequestGlobalFilter();
    }


}
