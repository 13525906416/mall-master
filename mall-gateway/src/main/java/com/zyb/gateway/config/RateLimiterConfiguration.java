package com.zyb.gateway.config;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;

import java.util.Objects;

/**
 * @ClassName RateLimiterConfiguration
 * @Author zhaoyb
 * @Date 2023/6/26
 * @Version 1.0
 */
@Configuration(proxyBeanMethods = false)
public class RateLimiterConfiguration {
    @Bean
    public KeyResolver remoteAddrKeyResolver() {
        return exchange -> Mono
                .just(Objects.requireNonNull(Objects.requireNonNull(exchange.getRequest().getRemoteAddress()))
                        .getAddress().getHostAddress());
    }

}
