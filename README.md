### 核心依赖

| 依赖                   | 版本         |
| ---------------------- |------------|
| Spring Boot            | 2.6.6      |
| Spring Cloud           | 2021.0.1   |
| Spring Cloud Alibaba   | 2021.0.1.0 |
| Mybatis Plus           | 3.5.2      |
| hutool                 | 5.8.10     |


### 模块说明

```lua
mall-master
└── mall-common -- 系统公共模块
├── mall-coupon -- 优惠券 Server[11001]
├── mall-member -- 会员 Server[11002]
├── mall-order -- 订单 Server[11003]
├── mall-product -- 商品 Server[11004]
├── mall-ware -- 仓储 Server[11005]
├── mall-admin -- 管理 Server[11006]
├── mall-thirdparty -- 第三方服务[111007]
├── mall-generator -- 代码生成器 Server[80]
├── mall-gateway -- Spring Cloud Gateway网关[9999]

数据库备份
mysqldump -u root -p --default-character-set UTF8 --databases gulimall_admin gulimall_config gulimall_sms gulimall_ums gulimall_wms gulimall_pms gulimall_oms > mysql_mall_all.sql



2. 欢迎提交 [issue](https://gitlab.com/13525906416/mall-master/issues)，请写清楚遇到问题的原因、开发环境、复显步骤。

3. 联系作者 <a href="mailto:zyb068@aliyun.com">zyb068@aliyun.com</a>
