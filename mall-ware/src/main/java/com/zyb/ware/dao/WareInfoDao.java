package com.zyb.ware.dao;

import com.zyb.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author zhaoyb
 * @email zyb068@aliyun.com
 * @date 2023-06-26 22:06:35
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
