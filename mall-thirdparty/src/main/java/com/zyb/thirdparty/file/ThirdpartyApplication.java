package com.zyb.thirdparty.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName ThirdpartyApplication
 * @Author zhaoyb
 * @Date 2023/8/21
 * @Version 1.0
 */
@SpringBootApplication
public class ThirdpartyApplication {
    public static void main(String[] args) {
        SpringApplication.run(ThirdpartyApplication.class,args);
    }
}
