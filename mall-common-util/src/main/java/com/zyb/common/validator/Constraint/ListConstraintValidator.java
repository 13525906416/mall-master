package com.zyb.common.validator.Constraint;

import com.zyb.common.validator.annotation.ListValue;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;

/**
 * @ClassName ListConstraintValidator
 * @Author zhaoyb
 * @Date 2023/8/28
 * @Version 1.0
 * 自定义校验器
 */
public class ListConstraintValidator implements ConstraintValidator<ListValue,Integer> {
    private Set<Integer> set = new HashSet<>();
    /**
     * 初始化
     * @param constraintAnnotation annotation instance for a given constraint declaration
     */
    @Override
    public void initialize(ListValue constraintAnnotation) {
        int[] values = constraintAnnotation.vals();
        if (values ==null || values.length == 0){
            return;
        }
        for (int val : values){
            set.add(val);
        }
    }

    /**
     * 是否校验成功
     * @param value object to validate
     * @param context context in which the constraint is evaluated
     *
     * @return
     */
    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        return set.contains(value);
    }
}
