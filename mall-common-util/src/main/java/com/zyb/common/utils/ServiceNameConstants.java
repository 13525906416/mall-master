package com.zyb.common.utils;

/**
 * @ClassName ServiceNameConstants
 * @Author zhaoyb
 * @Date 2023/7/2
 * @Version 1.0
 */
public interface ServiceNameConstants {
    /**
     * 优惠券服务的SERVICEID
     */
    String MALL_COUPON = "mall-coupon";

    /**
     * 会员服务的SERVICEID
     */
    String MALL_MEMBER = "mall-member";

    /**
     * 商品服务的SERVICEID
     */
    String MALL_PRODUCT = "mall-product";


    /**
     * 库存服务的SERVICEID
     */
    String MALL_WARE = "mall-ware";

    /**
     * 订单服务的SERVICEID
     */
    String MALL_ORDER = "mall-order";
}
