package com.zyb.order.dao;

import com.zyb.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author zhaoyb
 * @email zyb068@aliyun.com
 * @date 2023-06-26 22:10:47
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
