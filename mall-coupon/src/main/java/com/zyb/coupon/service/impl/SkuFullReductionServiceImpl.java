package com.zyb.coupon.service.impl;

import com.zyb.coupon.entity.MemberPriceEntity;
import com.zyb.coupon.entity.SkuLadderEntity;
import com.zyb.coupon.entity.to.SkuReductionTO;
import com.zyb.coupon.entity.vo.MemberPrice;
import com.zyb.coupon.service.MemberPriceService;
import com.zyb.coupon.service.SkuLadderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyb.common.utils.PageUtils;
import com.zyb.common.utils.Query;

import com.zyb.coupon.dao.SkuFullReductionDao;
import com.zyb.coupon.entity.SkuFullReductionEntity;
import com.zyb.coupon.service.SkuFullReductionService;


@Service("skuFullReductionService")
public class SkuFullReductionServiceImpl extends ServiceImpl<SkuFullReductionDao, SkuFullReductionEntity> implements SkuFullReductionService {

    @Autowired
    private SkuLadderService skuLadderService;
    @Autowired
    private MemberPriceService memberPriceService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuFullReductionEntity> page = this.page(
                new Query<SkuFullReductionEntity>().getPage(params),
                new QueryWrapper<SkuFullReductionEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveSkuReduction(SkuReductionTO skuReductionTO) {
        //1.保存优惠信息以及满减信息  openfeign调用sms服务 sms_sku_ladder\sms_sku_full_reduction\sms_member_price
        SkuLadderEntity skuLadderEntity = new SkuLadderEntity();
        skuLadderEntity.setSkuId(skuReductionTO.getSkuId());
        skuLadderEntity.setDiscount(skuReductionTO.getDiscount());
        skuLadderEntity.setFullCount(skuReductionTO.getFullCount());
        skuLadderEntity.setAddOther(skuReductionTO.getCountStatus());
//        skuLadderEntity.setPrice();
        if (skuLadderEntity.getFullCount().compareTo(new BigDecimal(0)) ==1){
            skuLadderService.save(skuLadderEntity);
        }


        SkuFullReductionEntity skuFullReductionEntity = new SkuFullReductionEntity();
        BeanUtils.copyProperties(skuReductionTO,skuFullReductionEntity);
        skuFullReductionEntity.setAddOther(skuReductionTO.getPriceStatus());
        if (skuFullReductionEntity.getFullPrice().compareTo(new BigDecimal(0))==1){
            this.save(skuFullReductionEntity);
        }

        List<MemberPrice> memberPrices = skuReductionTO.getMemberPrices();
        List<MemberPriceEntity> priceEntityList = memberPrices.stream()
        .filter(item ->{
            return item.getPrice().compareTo(new BigDecimal(0)) ==1;
        }).map(item -> {
            MemberPriceEntity memberPriceEntity = new MemberPriceEntity();
//            BeanUtils.copyProperties(item, memberPriceEntity);
            memberPriceEntity.setSkuId(skuReductionTO.getSkuId());
            memberPriceEntity.setMemberPrice(item.getPrice());
            memberPriceEntity.setMemberLevelId(item.getId());
            memberPriceEntity.setMemberLevelName(item.getName());
            memberPriceEntity.setAddOther(1);
            return memberPriceEntity;
        }).collect(Collectors.toList());
        memberPriceService.saveBatch(priceEntityList);
    }

}