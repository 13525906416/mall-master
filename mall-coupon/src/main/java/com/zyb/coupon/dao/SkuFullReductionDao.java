package com.zyb.coupon.dao;

import com.zyb.coupon.entity.SkuFullReductionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品满减信息
 * 
 * @author zhaoyb
 * @email zyb068@aliyun.com
 * @date 2023-06-26 21:48:47
 */
@Mapper
public interface SkuFullReductionDao extends BaseMapper<SkuFullReductionEntity> {
	
}
